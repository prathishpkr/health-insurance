package com.health.insurance;

public class Habits {

	boolean hypertension;
	boolean overweight;
	boolean smoking;
	boolean alcohol;
	boolean drugs;

	
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohol() {
		return alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isOverweight() {
		return overweight;
	}
	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}
	boolean bloodPressure;
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isSugar() {
		return sugar;
	}
	public void setSugar(boolean sugar) {
		this.sugar = sugar;
	}
	boolean sugar;
}
