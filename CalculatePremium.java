package com.health.insurance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CalculatePremium {

	int basePremium = 5000;
	double updatePremium;
	List<Double> premium = null;
	Map<String, Person> premiumCal = null;

	Map<String, Person> getPremiumByAge(List<Person> person) {

		// System.out.println("----"+person.size());
		premiumCal = new HashMap<String, Person>();
		for (Person p : person) {
			//System.out.println(p.getName());

			if (p.getAge() > 18 && p.getAge() < 40) {
				if (p.getGender().equalsIgnoreCase("Male"))
					p.setTotalPremium((basePremium * 0.1) + basePremium + (basePremium * 0.02));
				else
					p.setTotalPremium((basePremium * 0.1) + basePremium);

				premiumCal.put(p.getName(), p);
			}
			
			else {
				if (p.getGender().equalsIgnoreCase("Male"))
					p.setTotalPremium((basePremium * 0.2) + basePremium + (basePremium * 0.02));
				else
					p.setTotalPremium((basePremium * 0.2) + basePremium);

				premiumCal.put(p.getName(), p);
			}

		}
		
		return premiumCal;
	}

	Map<String, Person> updatePremiumByHabitHealth(Map<String, Person> premiumCal) {

		Map<String, Person> updatedPremiumCal = new HashMap<String, Person>();
		Habits habit = new Habits();
		Person person = new Person();
		updatedPremiumCal = premiumCal;
		
		//System.out.println(updatedPremiumCal.size());
		// loop a Map
		for (Map.Entry<String, Person> entry : premiumCal.entrySet()) {
			updatedPremiumCal = new HashMap<String, Person>();
			//System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
			person = new Person();
			person = entry.getValue();
			
			//System.out.println(person.getTotalPremium()+"----"+person.getHabit().isBloodPressure());
		if(person.getHabit().isHypertension() || person.getHabit().isBloodPressure() || person.getHabit().isOverweight() || habit.isSugar()){
			System.out.println("----------1");
				person.setTotalPremium((person.getTotalPremium() * 0.01) + person.getTotalPremium());
				updatedPremiumCal.put(entry.getKey(), person);
			}
			
		if (person.getHabit().isAlcohol() || person.getHabit().isDrugs() || person.getHabit().isSmoking()){
			System.out.println("----------2");
				person.setTotalPremium((person.getTotalPremium() * 0.03) + person.getTotalPremium());
				updatedPremiumCal.put(entry.getKey(), person);
			}
		
		 if(!person.getHabit().isAlcohol() && !person.getHabit().isDrugs() && !person.getHabit().isSmoking() && !person.getHabit().isHypertension() && !person.getHabit().isBloodPressure() && !person.getHabit().isOverweight()  && !person.getHabit().isSugar() ){
			System.out.println("----------3");
				person.setTotalPremium(person.getTotalPremium() - (person.getTotalPremium() * 0.03));
				updatedPremiumCal.put(entry.getKey(), person);
			}
			
			
		}
		
	 if(null != updatedPremiumCal && updatedPremiumCal.size() == 0)
			updatedPremiumCal = premiumCal;

		return updatedPremiumCal;
	}

}
